# Jill Beck: Technical Writing Samples

Welcome! Thank you for visiting my page. 

The following two technical writing samples are currently available. While I'm unable to provide writing samples from my current role due to confidentiality agreements, I'm happy to provide additional writing samples upon request. Please contact me at JillianLBeck@gmail.com at any time.

You can also access and download a PDF of the following writing samples: [Jill Beck: Technical Writing Samples](technical-writing/Jill_Beck_Writing_Samples.pdf).

## [The Style Guide Experiment](technical-writing/style-guide-experiment.md)

The following two writing samples describe the same tasks of adding, resolving, and deleting comments in Microsoft Word to demonstrate my ability to shift between different writing styles.

## [Including Excluded Heading Levels in Mini-TOCs (MadCap Flare)](technical-writing/including-excluded-heading-levels.md)

This writing sample describes a common MadCap Flare problem where some tables of contents (TOCs) do not generate as intended in PDF exports. Using my knowledge of HTML and CSS, I experimented with the project’s stylesheet until I successfully implemented a solution. Afterward, I created this document for the other technical writers on my team to explain why this problem occurs and how to resolve it.