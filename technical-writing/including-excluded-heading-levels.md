# Including Excluded Heading Levels in Mini-TOCs (MadCap Flare)

This document explains how to resolve situations where certain heading levels in Mini-TOCs do not display in a PDF export due to their exclusion from the General TOC.

## Prerequisites

This document requires a basic understanding of heading levels. For more information about heading levels, refer to the [Headings](https://www.w3.org/WAI/tutorials/page-structure/headings/) page from the Web Accessibility Initiative (WAI).

This document also requires a basic understanding of and the ability to edit Cascading Style Sheets (CSS). The following image defines basic CSS syntax for reference.

![#](images/madcap-flare-TOCs/basic-css-syntax.png)
 
## Background Information

MadCap Flare allows users to generate tables of contents (TOCs) that provide lists of the available content in a project. In a final export, the TOCs display the titles of the topics as clickable links to allow end users to easily navigate the content. The appearances of the titles depend on their defined heading levels (1-6). A user can edit the stylesheet to format how the heading levels display in the TOCs.

![#](images/madcap-flare-TOCs/general-toc-example.png)

A project can contain one TOC or multiple TOCs depending on the project’s scope. MadCap Flare has two types of TOCs:

- **General TOCs** are typically located at the beginning of a project. General TOCs list the heading levels of the topics throughout the entire document.

- **Mini-TOCs** can be inserted in multiple places throughout the project. Mini-TOCs list the heading levels of the specific sections or subsections to further organize information.

    ![#](images/madcap-flare-TOCs/general-and-mini-toc-example.png)

The following image shows an example of a General TOC at the beginning of a document and one Mini-TOC added into an example topic (Topic C) to create a list of the following subsections. Since Topic C is a second heading level, all lower-ranked heading levels (3-6) display in the generated Mini-TOC.
 
## The Current Approach and Its Limitations

In the General TOC, a MadCap Flare user has the option to exclude specific heading levels from the end user’s display. A user may choose to exclude specific heading levels to display only the top-level content. For example, a user can choose to only include heading levels 1-3 in the General TOC, excluding heading levels 4-6 from the end user’s view. Any topics excluded from the TOC are still included in the overall document.

In the stylesheet, this approach uses the mc-heading-level property and numeric values 1-6, depending on the desired heading level (h1-h6) in the TOC. The mc-heading-level property determines if a specific heading level is included in the TOC. If a user wants to exclude a heading level, the numeric value must be 0. Setting the mc-heading-level to 0 removes the selected heading levels from the TOC, therefore excluding it from the end user’s display. The mc-heading-level property applies to the heading levels for all General TOCs and Mini-TOCs.

The following example shows a project stylesheet and its generated General TOC that displays heading levels 1 and 2 and excludes heading levels 3, 4, 5, and 6. 

```
h1 {
    mc-heading-level: 1;
}

h2 {
    mc-heading-level: 2;
}

h3 {
    mc-heading-level: 0;
}

h4 {
    mc-heading-level: 0;
}

h5 {
    mc-heading-level: 0;
}

h6 {
    mc-heading-level: 0;
}
```

![#](images/madcap-flare-TOCs/h1-h2-toc-example.png)

The problem with this approach occurs when a user wants a Mini-TOC to display the excluded heading levels. Using the previous example, if a user inserts a Mini-TOC on a topic at the third heading level, MadCap Flare does not generate the titles of the topics at the fourth, fifth, and sixth heading levels. These heading levels are removed from the code of all TOCs, as previously defined by the mc-heading-level properties in the stylesheet. In these instances, the Mini-TOC does not display.

![#](images/madcap-flare-TOCs/missing-mini-toc-example.png)

## The Solution

The current approach uses the mc-heading-level property to target specific heading levels, which allows a user to omit or include these heading levels in both the General TOCs and Mini-TOCs. Instead, a user can target only the General TOC’s code to display or hide specific heading levels. To do this, use the **table.GenTOCTable*x*** selector (where *x* represents the numeric values 1-6 that correspond to the six heading levels) with the **display: none** declaration. This declaration hides certain heading levels from only the General TOC instead of completely removing them from all TOCs, meaning the heading levels still appear in the Mini-TOCs.

### Detailed Instructions

1.	In the **Content Explorer**, right-click on the project’s stylesheet. Select **Open with > Internal Text Editor** to open the stylesheet in the text editor.

    **NOTE**: By default, the stylesheet is located within the **Resources > Stylesheets** folder in the Content Explorer.

    ![#](images/madcap-flare-TOCs/open-with-internal-text-editor.png)
 
    **NOTE**: If your project has web and print styles within the same stylesheet, enter the code described in the following step within the print controls section. All print controls are contained in the @media print selector.

    ![#](images/madcap-flare-TOCs/media-print-controls.png)
 
1. Enter the **mc-heading-level** properties with values **1** through **6** for each of the heading selectors (h1-h6). This includes all the heading levels in the code of the General TOCs and Mini-TOCs.

    ```
    h1 {
        mc-heading-level: 1;
    }

    h2 {
        mc-heading-level: 2;
    }

    h3 {
        mc-heading-level: 3;
    }

    h4 {
        mc-heading-level: 4;
    }

    h5 {
        mc-heading-level: 5;
    }

    h6 {
        mc-heading-level: 6;
    }
    ```

1. Enter a new **table.GenTOCTable*x*** selector where x is the desired heading level excluded from the General TOC. The following example shows the selector with the value 4, which corresponds to heading level 4.

    **NOTE**: This selector is case-sensitive.

1. Enter the **display: none** declaration. This declaration hides the selected heading level from the General TOC’s display. The heading levels remain displayed in the Mini-TOC.

    ```
    table.GenTOCTable4 {
        display: none;
    }
    ```

The following example shows a stylesheet where heading levels 3, 4, 5, and 6 are excluded from the General TOC but are still displayed in any Mini-TOCs.

**NOTE**: Do not set any display declarations for heading levels you wish to include.

```
table.GenTOCTable3 {
    display: none;
}

table.GenTOCTable4 {
    display: none;
}

table.GenTOCTable5 {
    display: none;
}

table.GenTOCTable6 {
    display: none;
}
```

In the PDF export, the General TOC only shows topics with heading levels 1 and 2, and the Mini-TOC generates as intended with heading levels 3-6. 

![#](images/madcap-flare-TOCs/correctly-generated-toc.png)
 
**NOTE**: To omit heading levels from the Mini TOC that are included in the General TOC, use the **table.MiniTOCTable*x*** selector in place of the **table.GenTOCTable*x*** selector. This selector targets only the code of the Mini-TOC.