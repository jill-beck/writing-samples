# The Style Guide Experiment (Microsoft Word)

The following two approaches describe the same tasks of adding, resolving, and deleting comments in Microsoft Word to demonstrate my ability to shift between different writing styles.

## [Approach 1](#adding-resolving-and-deleting-comments-approach-1)

This approach follows guidelines similar to the [Microsoft Writing Style Guide](https://learn.microsoft.com/en-us/style-guide/welcome/). These guidelines:

- Use a more friendly, impersonal approach to make writing appear more human (contractions and personal pronouns are utilized).

- Encourage brevity and simplicity, meaning a writer can combine steps where relevant.

- Describe the action the user takes instead of describing the user interface (e.g. “Enter your comment” instead of “In the comment balloon, enter your comment”). A writer may omit words like “button” or “dialog box” in this approach.

A writer may choose this approach when communicating with end users to appear more personable while maintaining clarity.

## [Approach 2](#adding-resolving-and-deleting-comments-in-microsoft-word-approach-2)

This approach uses standard academic style guides. These guidelines:

- Use a more formal approach that eliminates personal pronouns and contractions.

- Separate each action into an individual step, which can help slow the end user’s reading speed.

- Describe the user interface more in-depth and explain what the software does after each user action.

A writer may choose this approach when communicating how specific software responds to user input.

## Adding, resolving, and deleting comments (Approach 1)

*Word for Microsoft 365 (Modern Comments)*

1. Select a specific word, sentence, or paragraph where you want to leave your comment.

1. On the **Insert tab**, select Comment.

    ![#](/technical-writing/images/style-guide-experiment/A1-1.png)

1. Enter your comment.

    ![#](/technical-writing/images/style-guide-experiment/A1-2.png)

1. Select **Post comment**.

    ![#](/technical-writing/images/style-guide-experiment/A1-3.png)

### Resolving a comment

1. Choose the comment you want to resolve.

1. Select **More thread options > Resolve thread**.

    ![#](/technical-writing/images/style-guide-experiment/A1-4.png)

**NOTE**: Open the **Comments** pane to view resolved comments.

![#](/technical-writing/images/style-guide-experiment/A1-5.png)

### Deleting a comment

1. Choose the comment you want to delete.

1. Select **More thread options > Delete thread**.

    ![#](/technical-writing/images/style-guide-experiment/A1-6.png)

## Adding, Resolving, and Deleting Comments in Microsoft Word (Approach 2)

The following procedures explain how to add, resolve, and delete comments in a Microsoft Word document. These instructions apply to Word for Microsoft 365.

### Adding a New Comment

1. Select a word, sentence, or paragraph.

1. Select the **Insert** tab.

    ![#](/technical-writing/images/style-guide-experiment/A2-1.png)

1. Select the **Comment** button.

    ![#](/technical-writing/images/style-guide-experiment/A2-2.png)

    A comment balloon appears.

    ![#](/technical-writing/images/style-guide-experiment/A2-3.png)

1. In the **Start a conversation** field, enter the desired comment.

    ![#](/technical-writing/images/style-guide-experiment/A2-4.png)

1. Select the **Post Comment** button.

    ![#](/technical-writing/images/style-guide-experiment/A2-5.png)

    The comment posts in the selected location.

    ![#](/technical-writing/images/style-guide-experiment/A2-6.png)

### Resolving a Comment

1. Select the **More thread options** button.

     ![#](/technical-writing/images/style-guide-experiment/A2-7.png)

    The **More thread options** dialog box appears.

     ![#](/technical-writing/images/style-guide-experiment/A2-8.png)

1. Select the **Resolve** thread button.

     ![#](/technical-writing/images/style-guide-experiment/A2-9.png)

    The comment resolves.
    
    ![#](/technical-writing/images/style-guide-experiment/A2-10.png)

### Viewing Resolved Comments

1. Select the **Comments** button.

     ![#](/technical-writing/images/style-guide-experiment/A2-11.png)

    The **Comments** pane opens, and all open and resolved comments appear.

      ![#](/technical-writing/images/style-guide-experiment/A2-12.png)

### Deleting an Existing Comment

**NOTE**: Deleting a comment removes its contents from the document. To hide the comment and save it for future reference, resolve the comment instead of deleting it. Refer to [Resolving a Comment](#viewing-resolved-comments) for more information.

1. Select the **More thread options** button.

      ![#](/technical-writing/images/style-guide-experiment/A2-13.png)

    The **More thread options** dialog box appears.

    ![#](/technical-writing/images/style-guide-experiment/A2-14.png)

1. Select the **Delete thread** button.

    ![#](/technical-writing/images/style-guide-experiment/A2-15.png)

    The comment balloon deletes.